﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{
    public class EnemyManager : MonoBehaviour
    {
        //Data Structures

        [System.Serializable]
        public class Pool
        {
            public EnemyType tag;
            public GameObject prefab;
            public int size;
        }

        public enum EnemyType
        {
            Lumberjack = 1,
            FireLord = 2,
            Constructor = 3
        }

        [System.Serializable]
        struct EnemyWave
        {
            public EnemyType[] enemies;
        }

        public delegate void WaveEvent();

        public static event WaveEvent WaveEnemiesDepleted;




        //Attributes

        private static EnemyManager _instance;

        [SerializeField] EnemyWave[] waves;
        [SerializeField] Transform[] spawnPoints;
        [SerializeField] List<Pool> pools;


        Dictionary<EnemyType, Queue<Enemy>> enemyPoolDict;
        Enemy[] enemyList;
        int currentWave;
        int enemiesOut;


        //Properties



        public static EnemyManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public Dictionary<EnemyType, Queue<Enemy>> EnemyPoolDict
        {
            get
            {
                return enemyPoolDict;
            }
        }

        public int EnemiesOut
        {
            get
            {
                return enemiesOut;
            }
            set
            {
                enemiesOut = value;
                LevelManager.Instance.UIControllerRef.RemainingEnemies.text = enemiesOut.ToString();
                if (enemiesOut <= 0)
                    WaveEnemiesDepleted();
            }
        }




        //Methods



        //Singleton
        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        void Start()
        {
            currentWave = 0;
            enemiesOut = 0;

            enemyPoolDict = new Dictionary<EnemyType, Queue<Enemy>>();

            InitializeWaves(30);

            foreach (Pool pool in pools)
            {
                Queue<Enemy> enemyPool = new Queue<Enemy>();

                pool.size = MaxWaveCount(pool.tag);

                for (int i = 0; i < pool.size; ++i)
                {
                    Enemy enemy = Instantiate(pool.prefab).GetComponent<Enemy>();
                    enemy.gameObject.SetActive(false);
                    enemyPool.Enqueue(enemy);
                }
                enemyPoolDict.Add(pool.tag, enemyPool);
            }
            //StartCoroutine(SpawnWave(currentWave));

        }


        IEnumerator SpawnWave()
        {
            LevelManager.Instance.UIControllerRef.TotalEnemies.text = waves[currentWave].enemies.Length.ToString();
            for (int i = 0; i < waves[currentWave].enemies.Length; ++i)
            {
                if (enemyPoolDict[waves[currentWave].enemies[i]].Count > 0)
                {
                    int spawnPointIndex = Random.Range(0, spawnPoints.Length);
                    Enemy enemy = enemyPoolDict[waves[currentWave].enemies[i]].Dequeue();
                    enemy.gameObject.SetActive(true);
                    enemy.transform.position = spawnPoints[spawnPointIndex].position;
                    enemy.transform.rotation = spawnPoints[spawnPointIndex].rotation;
                    EnemiesOut++;

                }

                yield return new WaitForSeconds(0.5f);
            }

            ++currentWave;
        }

        IEnumerator SpawnWave(bool randomWave, int numberOfEnemies)
        {
            
            EnemyWave wave = new EnemyWave();
            wave.enemies = new EnemyType[numberOfEnemies];
            for (int i = 0; i < wave.enemies.Length; i++)
            {
                yield return null;
            }
        }



        int MaxWaveCount(EnemyType type)
        {
            int max = 0;
            foreach (EnemyWave wave in waves)
            {
                int count = 0;
                foreach (EnemyType enemy in wave.enemies)
                {
                    if (enemy == type)
                        ++count;
                }
                max = count > max ? count : max;
            }
            return max;
        }

        public void ActionWave()
        {
            StartCoroutine(SpawnWave());
        }

        void InitializeWaves(int numberOfWaves)
        {
            waves = new EnemyWave[numberOfWaves];
            for (int i = 0; i < numberOfWaves; i++)
            {
                waves[i].enemies = new EnemyType[i * 5 + 5];
                for (int j = 0; j < waves[i].enemies.Length; ++j)
                {
                    if (Random.Range(0, 2) < 1)
                    {
                        waves[i].enemies[j] = EnemyType.Lumberjack;
                    }
                    else
                    {
                        waves[i].enemies[j] = EnemyType.FireLord;

                    }

                }
            }
        }
    }

}
