﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{
    public class Fireguy : Enemy
    {

        [SerializeField] float attackRange = 0.5f;


        void Start()
        {
            StartCoroutine(SearchProcess());
            navMeshAgent.stoppingDistance = attackRange;
            StartCoroutine(AttackProcess());
        }

        void Update()
        {

            if (targetTree != null)
            {
                navMeshAgent.destination = targetTree.transform.position;
            }

        }

        IEnumerator AttackProcess()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.5f);
                if (targetTree != null)
                {
                    if (Vector3.Distance(this.transform.position, targetTree.transform.position) < attackRange + 1)
                    {
                        Attack();
                    }

                }

            }

        }

        void Attack()
        {
            targetTree.GetComponent<Tree>().ApplyDamage(attackPower);
            StopAllCoroutines();
            Destroy();
        }

        protected override void Destroy()
        {
            _hitpoints = hitPoints;
            Instantiate(deathPrefab, this.transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
            EnemyManager.Instance.EnemyPoolDict[EnemyManager.EnemyType.FireLord].Enqueue(this);
            EnemyManager.Instance.EnemiesOut--;
        }
    }
}
