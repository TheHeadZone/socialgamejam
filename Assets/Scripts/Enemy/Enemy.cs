﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

namespace SocialJam
{

    public abstract class Enemy : MonoBehaviour
    {
        [SerializeField]protected int hitPoints;
        protected int _hitpoints;
        
        [SerializeField]protected int attackPower;
        [SerializeField]protected int movementSpeed;
        [SerializeField]protected GameObject deathPrefab;
        protected GameObject targetTree;

		protected NavMeshAgent navMeshAgent;

		void Awake(){
			this.navMeshAgent = GetComponent<NavMeshAgent>();
			this.navMeshAgent.speed = movementSpeed;
            _hitpoints = hitPoints;
			
		}

        protected virtual void SearchTargetTree()
        {
            //Find a tree
            float minDistance = Mathf.Infinity;
            foreach (GameObject tree in ConstructionManager.Instance.TreeArray)
            {
				if(tree == null)
					continue;
                float currentDistance = Vector3.Distance(this.transform.position, tree.transform.position);
                if (currentDistance < minDistance)
                {
                    targetTree = tree;
                    minDistance = currentDistance;
                }
            }
        }

        protected IEnumerator SearchProcess()
        {
            while (true)
            {
                if (targetTree == null)
                {
                    SearchTargetTree();
                }
                yield return new WaitForSeconds(0.5f);

            }
        }

        public void ApplyDamage(int damage){
            this._hitpoints -= damage;
            if(_hitpoints <= 0){
                Destroy();
            }
        }

        protected abstract void Destroy();

    }

}

