﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SocialJam
{

    public class Lumberjack : Enemy
    {

        [SerializeField] float attackRange = 0.5f;
        bool onCoolDown = false;


        void Start()
        {
            StartCoroutine(SearchProcess());
            navMeshAgent.stoppingDistance = attackRange;
            StartCoroutine(AttackProcess());
        }

        void Update()
        {

            if (targetTree != null)
            {
                navMeshAgent.destination = targetTree.transform.position;
            }

        }

        IEnumerator AttackProcess()
        {
            while (true)
            {
                if (targetTree != null)
                {
                    if (Vector3.Distance(this.transform.position, targetTree.transform.position) < attackRange + 1)
                    {
                        Attack();
                    }
                }
                yield return new WaitForSeconds(0.5f);
            }
        }

        void Attack()
        {
            if (!onCoolDown)
            {
                targetTree.GetComponent<Tree>().ApplyDamage(attackPower);
                StartCoroutine(ApplyCooldown());
            }
        }

        IEnumerator ApplyCooldown()
        {
            onCoolDown = true;
            yield return new WaitForSeconds(2.5f);
            onCoolDown = false;
        }


        override protected void Destroy(){
            _hitpoints = hitPoints;
            Instantiate(deathPrefab, this.transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);   
            EnemyManager.Instance.EnemyPoolDict[EnemyManager.EnemyType.Lumberjack].Enqueue(this);
            EnemyManager.Instance.EnemiesOut--;
        }
    }

}
