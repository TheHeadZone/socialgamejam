﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace SocialJam
{

    public class ConstructionManager : MonoBehaviour
    {
        //Attributes

        static ConstructionManager instance;
        List<GameObject> treeArray;
        List<GameObject> buildingArray;

        [SerializeField] GameObject[] treePrefab;


        //Properties

        public static ConstructionManager Instance
        {
            get
            {
                return instance;

            }
        }

        public List<GameObject> TreeArray
        {
            get
            {
                return treeArray;
            }
        }

        public List<GameObject> BuildingArray
        {
            get
            {
                return buildingArray;
            }
        }


        //Methods

        void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                instance = this;
            }



        }

        void Start()
        {
            treeArray = GameObject.FindGameObjectsWithTag("Tree").ToList();
            foreach (GameObject tree in treeArray)
            {
                tree.GetComponent<Tree>().StartIncreaseScore();
            }
            buildingArray = new List<GameObject>();
        }

        void Update()
        {
            if (treeArray.Count <= 0)
            {
                print("Perdiste! :c Tus arboles llegaron a 0");
            }
        }



        public void InstantiateTree()
        {
            if (LevelManager.Instance.Score >= LevelManager.Instance.PricePerTree)
            {
                Instantiate(treePrefab[Random.Range(0, treePrefab.Length)]).GetComponent<Tree>().IsActive = false;
                LevelManager.Instance.Score -= LevelManager.Instance.PricePerTree;
            }
        }

    }

}
