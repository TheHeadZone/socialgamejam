﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{

    public class Construction : MonoBehaviour
    {

		//Attributes

        [SerializeField] protected float range = 10;
        [SerializeField] protected int hitPoints = 3;

        [SerializeField] protected GameObject damageParticles;
        [SerializeField] protected GameObject destructionParticles;



        //Properties



        //Methods

        void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(this.transform.position, range);
        }
    }

}

