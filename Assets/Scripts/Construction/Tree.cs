﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SocialJam
{

    public class Tree : Construction
    {
        bool isActive = true;

        [SerializeField] float treeSeparation;
        [SerializeField] float increaseScoreTime;


        public bool IsActive{
            get{
                return isActive;
            }
            set{
                isActive = value;
            }
        }


        void Start()
        {
            //isActive = false;


        }

        void Update()
        {
            if(EventSystem.current.IsPointerOverGameObject())
                return;
            if (!isActive)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Grass")))
                {
                    this.transform.position = hit.point;
                    if (Input.GetMouseButtonDown(0) && CheckIfValidPosition())
                    {
                        isActive = true;
                        ConstructionManager.Instance.TreeArray.Add(this.gameObject);
                        StartCoroutine(IncreaseScore());
                    }

                }
            }

        }


        public void ApplyDamage(int damage)
        {
            Instantiate(damageParticles, this.transform.position, Quaternion.identity);
            hitPoints -= damage;
            if (hitPoints <= 0)
            {
                DestroyTree();
            }
        }

        void DestroyTree()
        {
            Instantiate(destructionParticles, this.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            ConstructionManager.Instance.TreeArray.Remove(this.gameObject);
        }

        bool CheckIfValidPosition()
        {
            foreach (GameObject building in ConstructionManager.Instance.BuildingArray)
            {
                if (Vector3.Distance(this.transform.position, building.transform.position) < range)
                    return false;
            }
            foreach (GameObject tree in ConstructionManager.Instance.TreeArray)
            {
                if (Vector3.Distance(this.transform.position, tree.transform.position) < treeSeparation)
                    return false;
            }
            return true;
        }

        IEnumerator IncreaseScore(){
            yield return new WaitForSeconds(increaseScoreTime);
            while(true){
                LevelManager.Instance.Score++;
                yield return new WaitForSeconds(increaseScoreTime);
            }
        }

        public void StartIncreaseScore(){
            StartCoroutine(IncreaseScore());
        }
    }
}

