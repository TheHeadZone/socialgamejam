﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SocialJam
{

    public class UIController : MonoBehaviour
    {
        [SerializeField] Text remainingEnemies;
        [SerializeField] Text energyPoints;
        [SerializeField] Text lives;
        [SerializeField] Text totalEnemies;



        public Text RemainingEnemies
        {
            get
            {
                return remainingEnemies;
            }
            set
            {
                remainingEnemies = value;
            }
        }



        public Text EnergyPoints
        {
            get
            {
                return energyPoints;
            }
            set
            {
                energyPoints = value;
            }
        }

        public Text TotalEnemies
        {
            get
            {
                return totalEnemies;
            }

            set
            {
                totalEnemies = value;
            }
        }

        public Text Lives
        {
            get
            {
                return lives;
            }

            set
            {
                lives = value;
            }
        }

        void OnGUI()
        {

        }

        void Start()
        {
            //remainingEnemies.text = "10";
        }


    }

}
