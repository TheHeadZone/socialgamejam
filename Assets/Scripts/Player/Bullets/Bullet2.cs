﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{
    public class Bullet2 : MonoBehaviour
    {

        public float speed;
        public float angleVar;
        public bool isCopy = false;
        private GameObject copy1, copy2, copy3, copy4;

        [SerializeField] int damage;

        void Start()
        {
            if (!isCopy)
            {
                copy1 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(this.transform.forward) * Quaternion.Euler(0, angleVar, 0));
                copy2 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(this.transform.forward) * Quaternion.Euler(0, -angleVar, 0));
                copy3 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(copy1.transform.forward) * Quaternion.Euler(0, angleVar, 0));
                copy4 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(copy2.transform.forward) * Quaternion.Euler(0, -angleVar, 0));

                copy1.GetComponent<Bullet2>().isCopy = true;
                copy2.GetComponent<Bullet2>().isCopy = true;
                copy3.GetComponent<Bullet2>().isCopy = true;
                copy4.GetComponent<Bullet2>().isCopy = true;
            }

            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 50, ForceMode.Impulse);

        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy"))
            {

                collider.GetComponent<Enemy>().ApplyDamage(damage);

            }
            Destroy(this.gameObject);
        }
    }



}

