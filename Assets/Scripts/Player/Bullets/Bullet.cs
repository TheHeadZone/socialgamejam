﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{

    public class Bullet : MonoBehaviour
    {
        [SerializeField] float speed;
        [SerializeField] int damage = 1;

        void Start()
        {
            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 50, ForceMode.Impulse);
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy"))
            {

                collider.GetComponent<Enemy>().ApplyDamage(damage);

            }
            Destroy(this.gameObject);
        }

        void OnCollisionEnter(Collision col){
            Destroy(this.gameObject);
        }
    }

}

