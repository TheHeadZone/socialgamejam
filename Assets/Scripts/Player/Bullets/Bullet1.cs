﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{
    public class Bullet1 : MonoBehaviour
    {

        public float speed;
        public float angleVar;
        public bool isCopy = false;
        private GameObject copy1, copy2;

        [SerializeField] int damage = 1;


        void Start()
        {
            if (!isCopy)
            {
                copy1 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(this.transform.forward) * Quaternion.Euler(0, angleVar, 0));
                copy2 = Instantiate(this.gameObject, this.transform.position, Quaternion.LookRotation(this.transform.forward) * Quaternion.Euler(0, -angleVar, 0));
                copy1.GetComponent<Bullet1>().isCopy = true;
                copy2.GetComponent<Bullet1>().isCopy = true;
            }

            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * speed, ForceMode.Impulse);
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy"))
            {

                collider.GetComponent<Enemy>().ApplyDamage(damage);

            }
            Destroy(this.gameObject);
        }
    }

}

