﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SocialJam
{

    public class Player : MonoBehaviour
    {

        [SerializeField] float speed;
        [SerializeField] float rotationSpeed;
        [SerializeField] GameObject head;
        [SerializeField] GameObject bulletPrefab;
        [SerializeField] Transform bulletSpawnPos;


        float mouseDeltaCumulation = 0;
        Vector2 mousePosition = new Vector2(0, 0);

        bool onCoolDown = false;

        // Update is called once per frame
        void Update()
        {
            float horAxis = Input.GetAxis("Horizontal");
            float verAxis = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(horAxis, 0, verAxis);
            this.transform.Translate(movement * speed * Time.deltaTime, Space.World);
            if (Mathf.Abs(horAxis) > 0 || Mathf.Abs(verAxis) > 0)
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(movement), Time.deltaTime * rotationSpeed);


            Vector2 currentMousePos = Input.mousePosition;
            mouseDeltaCumulation += Vector2.Distance(mousePosition, currentMousePos);
            mouseDeltaCumulation *= 0.5f;
            mousePosition = currentMousePos;

            if (mouseDeltaCumulation > 50)
            {
                print("it works!");
            }


            Ray ray = Camera.main.ScreenPointToRay(mousePosition);

            RaycastHit hit;
            int layerMask = LayerMask.NameToLayer("Floor");
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {

                Vector3 lookAtTarget = hit.point - this.transform.position;
                lookAtTarget.Scale(new Vector3(1, 0, 1));
                this.head.transform.rotation = Quaternion.Slerp(head.transform.rotation, Quaternion.LookRotation(lookAtTarget), Time.deltaTime * rotationSpeed);
            }


            if (Input.GetMouseButton(0))
            {
                StartCoroutine(Shoot());
            }
        }

        IEnumerator Shoot()
        {
            if (!(onCoolDown))
            {

                Instantiate(bulletPrefab, bulletSpawnPos.position, bulletSpawnPos.rotation);
                onCoolDown = true;
                yield return new WaitForSeconds(1f);
                onCoolDown = false;
            }
        }
    }

}
