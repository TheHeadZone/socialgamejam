﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTest : MonoBehaviour
{
    Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rend.material.SetInt("_Active", 1);

        }
        if (Input.GetMouseButtonUp(0))
        {
            rend.material.SetInt("_Active", 0);
        }
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            int layerMask = LayerMask.NameToLayer("Floor");
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                rend.material.SetVector("_Position", hit.point);
            }
        }



    }
}
