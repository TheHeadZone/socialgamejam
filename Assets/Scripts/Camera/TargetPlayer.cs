﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPlayer : MonoBehaviour {
	[SerializeField] Transform playerTransform;
	[SerializeField] Vector3 offset;

	void LateUpdate(){
		this.transform.position = new Vector3(playerTransform.position.x + offset.x,
		this.transform.position.y + offset.y,
		playerTransform.position.z + offset.z);
	}
	
}
