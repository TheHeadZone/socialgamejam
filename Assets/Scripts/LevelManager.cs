﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SocialJam
{
    public class LevelManager : MonoBehaviour
    {

        //Attributes
        private static LevelManager _instance;

        int score;
        int energyPoints;
        float timeRemaining;
        bool wavesStarted;

        [SerializeField] int pricePerPlayerUpgrade = 5;
        [SerializeField] int pricePerTree = 5;
        
        [SerializeField] int lives;

        [SerializeField] float timeBetweenWaves;

        [SerializeField] UIController uIController;

        //Properties
        public static LevelManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public UIController UIControllerRef
        {
            get
            {
                return uIController;
            }
        }

        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
                uIController.EnergyPoints.text = score.ToString();
            }
        }

        public int Lives{
            get{
                return lives;
            }
            set{
                lives = value;
                uIController.Lives.text = lives.ToString();
                if(lives <= 0)
                    print("Perdiste :c Tus vidas llegaron a 0");
            }
        }

        public int PricePerPlayerUpgrade{
            get{
                return pricePerPlayerUpgrade;
            }
        }
        public int PricePerTree{
            get{
                return pricePerTree;
            }
        }


        //Methods

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        void Start()
        {
            score = 0;
            energyPoints = 0;
            Lives = lives;
            
            EnemyManager.WaveEnemiesDepleted += StartTimer;
            wavesStarted = false;
            Invoke("SendWave", 3f);
        }
        void Update()
        {

            
        }

        public void SendWave()
        {
            EnemyManager.Instance.ActionWave();
            uIController.RemainingEnemies.text = EnemyManager.Instance.EnemiesOut.ToString();
        }

        void StartTimer(){
            timeRemaining = timeBetweenWaves;
            //uIController.TimeUntilNextWave.text = timeRemaining.ToString();
            StartCoroutine(CountToNextWave());
        }

        IEnumerator CountToNextWave(){
            while(timeRemaining > 0){
                timeRemaining -= Time.deltaTime;
                timeRemaining = timeRemaining > 0 ? timeRemaining : 0;
                //uIController.TimeUntilNextWave.text = timeRemaining.ToString("0.00");
                yield return null;
            }
            SendWave();
        }


    }

}


