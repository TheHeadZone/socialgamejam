﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animalito1Script : MonoBehaviour {
    
    [SerializeField] GameObject bulletPrefab;
    Transform bulletSpawnPos;
    

    bool onCoolDown = false;
    private void Start()
    {
        bulletSpawnPos = this.gameObject.transform;
    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            StartCoroutine(Shoot());
        }
    }

    IEnumerator Shoot()
    {
        if (!(onCoolDown))
        {

            Instantiate(bulletPrefab, bulletSpawnPos.position, bulletSpawnPos.rotation);
            onCoolDown = true;
            yield return new WaitForSeconds(0.5f);
            onCoolDown = false;
        }
    }
}
