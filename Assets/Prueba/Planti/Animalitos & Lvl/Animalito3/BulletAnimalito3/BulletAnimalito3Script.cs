﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SocialJam
{

    public class BulletAnimalito3Script : MonoBehaviour
    {

        public float speed;
        public float UpImpulse;

        public GameObject explotion;

        [SerializeField] int damage = 1;


        void Start()
        {

            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * speed + new Vector3(0, UpImpulse, 0), ForceMode.Impulse);
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy"))
            {

                collider.GetComponent<Enemy>().ApplyDamage(damage);

            }
            Destroy(this.gameObject);
        }

        void OnCollisionEnter(Collision c)
        {
            Instantiate(explotion, this.gameObject.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }


}

