﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAnimalito3ExplotionScript : MonoBehaviour {

    public float explotionTime;
    public float explotionSize;
	void Start () {
        StartCoroutine(SelfDestruct());
        this.gameObject.transform.localScale = new Vector3(explotionSize, explotionSize, explotionSize);
	}
    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Enemy"))
        {

            //Deal Damage

        }
        
    }
    IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(explotionTime);
        Destroy(this.gameObject);
    }

}
