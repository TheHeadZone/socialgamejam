﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SocialJam
{

    public class PlayerUpgradeScript : MonoBehaviour
    {
        int naturePoints;
        int playerLevel;
        int priceForLevel;
        public GameObject bullet2, bullet3, animalito1, animalito2, animalito3;
        // Use this for initialization
        void Start()
        {
            naturePoints = 0;
            playerLevel = 1;
            priceForLevel = 100;

            animalito1.active = false;
            animalito2.active = false;
            animalito3.active = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.G))
            {
                print("Points = " + naturePoints);
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                if (priceForLevel != 0)
                {
                    if (naturePoints >= priceForLevel)
                    {
                        LvlUp();
                    }
                    else print("Not enough points!");
                }
                else print("Max Level");

            }
        }

        private void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.G))
            {
                naturePoints += 2;
            }

        }

        private void LvlUp()
        {
            naturePoints -= priceForLevel;
            priceForLevel += 10;
            playerLevel += 1;
            print("Level Up! " + playerLevel);
            switch (playerLevel)
            {
                case 2:
                    animalito1.active = true;
                    break;
                case 3:
                    gameObject.GetComponent<PlayerTest>().bulletPrefab = bullet2;
                    break;
                case 4:
                    animalito2.active = true;
                    break;
                case 5:
                    gameObject.GetComponent<PlayerTest>().bulletPrefab = bullet3;
                    break;
                case 6:
                    animalito3.active = true;
                    priceForLevel = 0;
                    break;
            }
        }

        public void UpgradeLevel()
        {
            if (LevelManager.Instance.Score >= LevelManager.Instance.PricePerPlayerUpgrade)
            {
                
                LvlUp();
                LevelManager.Instance.Score -= LevelManager.Instance.PricePerPlayerUpgrade;
                
            }
        }

    }




}

