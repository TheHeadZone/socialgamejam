﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SocialJam
{

    public class BulletAnimalito2Script : MonoBehaviour
    {

        [SerializeField] float speed;
        [SerializeField] int damage = 1;

        void Start()
        {
            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * speed, ForceMode.Impulse);
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy"))
            {

                collider.GetComponent<Enemy>().ApplyDamage(damage);

            }
            Destroy(this.gameObject);
        }
    }

}


