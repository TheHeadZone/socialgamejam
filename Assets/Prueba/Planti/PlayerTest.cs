﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SocialJam
{
    public class PlayerTest : MonoBehaviour
    {

        [SerializeField] float speed;
        [SerializeField] float rotationSpeed;
        [SerializeField] GameObject head;
        [SerializeField] public GameObject bulletPrefab;
        [SerializeField] Transform bulletSpawnPos;
        [SerializeField] float inmunityDuration = 3f;

        //Create animator ****************
        Animator animator;
        //*********************

        bool inmuneToDamage = false;



        bool onCoolDown = false;

        //Initialize animator********************
        private void Start()
        {
            animator = GetComponent<Animator>();
        }
        //****************************************

        void Update()
        {
            float horAxis = Input.GetAxis("Horizontal");
            float verAxis = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(horAxis, 0, verAxis);

            //Movement behavior ********************
            if (horAxis != 0 || verAxis != 0)
            {
                animator.SetBool("isMoving", true);
            }
            else animator.SetBool("isMoving", false);

            //********************************************


            this.transform.Translate(movement * speed * Time.deltaTime, Space.World);
            if (Mathf.Abs(horAxis) > 0 || Mathf.Abs(verAxis) > 0)
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(movement), Time.deltaTime * rotationSpeed);



            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            int layerMask = LayerMask.GetMask("Floor");
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {

                Vector3 lookAtTarget = hit.point - this.transform.position;
                lookAtTarget.Scale(new Vector3(1, 0, 1));
                this.head.transform.rotation = Quaternion.Slerp(head.transform.rotation, Quaternion.LookRotation(lookAtTarget), Time.deltaTime * rotationSpeed);
            }


            if (Input.GetMouseButton(0))
            {
                StartCoroutine(Shoot());
            }
        }

        IEnumerator Shoot()
        {
            if (!(onCoolDown))
            {

                Instantiate(bulletPrefab, bulletSpawnPos.position, bulletSpawnPos.rotation);
                onCoolDown = true;
                yield return new WaitForSeconds(1f);
                onCoolDown = false;
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Enemy") && !inmuneToDamage)
            {
                ApplyDamage();
            }
        }

        void ApplyDamage()
        {
            LevelManager.Instance.Lives--;
            StartCoroutine(DamageCoolDown());
        }
        IEnumerator DamageCoolDown()
        {
            inmuneToDamage = true;
            yield return new WaitForSeconds(inmunityDuration);
            inmuneToDamage = false;
        }
    }



}

