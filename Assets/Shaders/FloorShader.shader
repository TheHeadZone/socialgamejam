﻿Shader "Custom/FloorShader" {
	Properties {
		_Albedo("Albedo", 2D) = "white"{}
		_Color("Floor Color", Color) = (1, 1, 1, 1)
		_Position("Position", Vector) = (0, 0, 0, 0)
		_Range("Range", Float) = 15
		_Tint("Tint", Color) = (1, 1, 1, 1)
		_Active("Is Active", Int) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _Albedo;

		struct Input {
			float2 uv_Albedo;
			float3 worldPos;
		};

		
		float4 _Position;
		float _Range;
		fixed4 _Tint;
		int _Active;
		fixed4 _Color;



		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = clamp(1-(distance(IN.worldPos, _Position)/_Range), 0, 1) * _Tint * _Active;
			//o.Albedo = c.rgb + tex2D(_Albedo, IN.uv_Albedo).rgb * (1 - c.rgb) / 2;
			o.Albedo = c.rgb + ((1 - c.rbg) * _Color);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
